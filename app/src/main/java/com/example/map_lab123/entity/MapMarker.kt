package com.example.map_lab123.entity

import android.net.Uri
import io.realm.RealmObject

// Класс для работы с объектом маркера (координаты + изображение)
open class MapMarker: RealmObject() {
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var imageUriStr: String? = null

    fun getLatitude(): Double {
        return latitude
    }

    fun setLatitude(latitude: Double) {
        this.latitude = latitude
    }

    fun getLongitude(): Double {
        return longitude
    }

    fun setLongitude(longitude: Double) {
        this.longitude = longitude
    }

    fun getImage(): Uri? {
        if (imageUriStr == null)
            return null
        return Uri.parse(imageUriStr)
    }

    fun setImage(image: Uri) {
        this.imageUriStr = image.toString()
    }
}