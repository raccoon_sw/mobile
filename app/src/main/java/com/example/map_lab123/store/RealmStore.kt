package com.example.map_lab123.store

import android.content.Context
import android.net.Uri
import android.util.Log
import com.example.map_lab123.entity.MapMarker
import com.google.android.gms.maps.model.LatLng
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import java.lang.Exception

// Класс для работы с базой данных
open class RealmStore() {
    private lateinit var realm: Realm
    private val logTag = "store"

    constructor(context: Context) : this() {
        Realm.init(context)
        Realm.removeDefaultConfiguration()
        Realm.setDefaultConfiguration(getDefaultConfig()!!)
        this.realm = Realm.getDefaultInstance()
         this.realm.beginTransaction()
         this.realm.deleteAll()
         this.realm.commitTransaction()
    }

    fun close() {
        if (checkConnectionIsOpen()) {
            this.realm.close()
        }
    }

    fun findAllMarkers(): RealmResults<MapMarker>? {
        if (!checkConnectionIsOpen()) {
            return null
        }
        return try {
            this.realm.where(MapMarker::class.java).findAll()
        } catch (e: Exception) {
            Log.e(logTag, "Markers are not found: $e.message")
            null
        }
    }

    fun createMarker(position: LatLng) {
        if (!checkConnectionIsOpen()) {
            return
        }
        try {
            this.realm.executeTransaction {
                val marker = MapMarker()
                marker.setLatitude(position.latitude)
                marker.setLongitude(position.longitude)
                this.realm.copyToRealm(marker)
            }
        } catch (e: Exception) {
            Log.e(logTag, "Marker is not created: $e.message")
        }
    }

    fun updateMarkerImage(position: LatLng, image: Uri) {
        if (!checkConnectionIsOpen()) {
            return
        }
        try {
            this.realm = Realm.getDefaultInstance()
            this.realm.executeTransaction {
                val marker = this.realm.where(MapMarker::class.java)
                    .equalTo("latitude", position.latitude)
                    .equalTo("longitude", position.longitude)
                    .findFirst()
                if (marker == null) {
                    Log.e(logTag, "Marker is not found")
                    return@executeTransaction
                }
                marker.setImage(image)
            }
        } catch (e: Exception) {
            Log.e(logTag, "Marker is not updated: $e.message")
        }
    }

    private fun checkConnectionIsOpen(): Boolean {
        return !this.realm.isClosed
    }

    fun getRealm(): Realm {
        return this.realm
    }

    private fun getDefaultConfig(): RealmConfiguration? {
        val schemaVersion: Long = 3
        return RealmConfiguration.Builder()
            .schemaVersion(schemaVersion)
            .deleteRealmIfMigrationNeeded()
            .build()
    }

}