package com.example.map_lab123.activities

import android.Manifest
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.os.StrictMode
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.example.map_lab123.R
import com.example.map_lab123.entity.MapMarker
import com.example.map_lab123.store.RealmStore
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import io.realm.kotlin.where
import java.io.Serializable
import java.util.*
import kotlin.math.atan
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


@Suppress("UNCHECKED_CAST")
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private /*lateinit*/ var mMarker: Marker? = null
    private lateinit var store: RealmStore
    private var mMarkersAndPhotos = HashMap<LatLng, Uri?>()
    private val logTag = "map-activity"

    private val markerShow = 1
    private val currentImage = "CurrentImage"
    private val markersState = "MasMarkers"

    private lateinit var locationRequest: LocationRequest
    private lateinit var locationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    fun calculateDistanceToMarker(mapMarker: MapMarker, location: Location): Double {
        val dl = mapMarker.getLongitude() - location.longitude
        val cosF1 = cos(mapMarker.getLatitude())
        val sinF1 = sin(mapMarker.getLatitude())
        val cosF2 = cos(location.latitude)
        val sinF2 = sin(location.latitude)
        val tmp1 = cosF2 * sin(dl)
        val tmp2 = cosF1 * sinF2 - sinF1 * cosF2 * cos(dl)

        // 111 км = 1 градус на суше
        return 111 * atan(
            sqrt(tmp1 * tmp1 + tmp2 * tmp2)
                    / (sinF1 * sinF2 + cosF1 * cosF2 * cos(dl))
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            Log.d(logTag, "No states")
        } else {
            Log.d(logTag, "State is restored")
        }
        store = RealmStore(this)
        setContentView(R.layout.activity_maps)
        createNotificationChannel()

        locationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10 * 1000
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations)
                    if (store.getRealm().where<MapMarker>().findAll().any { marker ->
                            calculateDistanceToMarker(marker, location) <= 0.5 //дистанция 0.5 км
                        })
                        with(NotificationManagerCompat.from(baseContext)) {
                            notify(
                                123, NotificationCompat.Builder(baseContext, "12345")
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle("Place notification")
                                    .setContentText("Some marker is close")
                                    .setStyle(
                                        NotificationCompat.BigTextStyle()
                                            .bigText("Some marker is close")
                                    )
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                    .build()
                            )
                        }
            }
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Place channel"
            val descriptionText = "Channel for place notifications"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("12345", name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            Log.e(logTag, "WOW. Some marker is close")
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMapClickListener { location ->
            mMarker = mMap.addMarker(
                MarkerOptions().position(location)
            )
            mMarkersAndPhotos[mMarker!!.position] = null
            mMarker!!.showInfoWindow()
            this.store.createMarker(location)
        }

        mMap.setOnMarkerClickListener(OnMarkerClickListener { marker ->
            if (mMarker != null && mMarker!!.isInfoWindowShown) {
                mMarker!!.hideInfoWindow()
            }
            if (marker != null) {
                mMarker = marker
                mMarker!!.showInfoWindow()
                onClickMarker()
                return@OnMarkerClickListener true
            }
            false
        })

        mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(58.01046, 56.25017)))

        if (mMarkersAndPhotos.count() == 0) {
            this.loadMarkersFromStore()
        }
        if (!requestPermissions()) {
            finish()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            outState.putSerializable(markersState, mMarkersAndPhotos as Serializable)
            super.onSaveInstanceState(outState)
        } catch (e: Exception) {
            Log.e(logTag, "ERROR. Save state: $e.message")
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        try {
            mMarkersAndPhotos =
                savedInstanceState.getSerializable(markersState) as HashMap<LatLng, Uri?>
            for ((key, _) in mMarkersAndPhotos) {
                mMap.addMarker(MarkerOptions().position(key))
            }
        } catch (e: Exception) {
            Log.e(logTag, "ERROR. Restore state: $e.message")
        }
    }

    private fun onClickMarker() {
        val intent = Intent(this, MarkerActivity::class.java)
        if (mMarkersAndPhotos[mMarker!!.position] != null) {
            intent.putExtra(currentImage, mMarkersAndPhotos[mMarker!!.position])
        }
        startActivityForResult(intent, markerShow)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestPermissions(): Boolean {
        val permissions = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
            , Manifest.permission.ACCESS_FINE_LOCATION
        )
        requestPermissions(permissions, 1)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        for (p in permissions) {
            if (ContextCompat.checkSelfPermission(this, p)
                != PackageManager.PERMISSION_GRANTED
            ) {
                Log.d(logTag, "ERROR. Required permissions are not granted")
                return false
            }
        }
        mMap.isMyLocationEnabled = true
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            markerShow ->
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val uri = data.getParcelableExtra<Uri>(currentImage)
                    if (uri != null && mMarkersAndPhotos.containsKey(mMarker!!.position)) {
                        mMarkersAndPhotos[mMarker!!.position] = uri
                        this.store.updateMarkerImage(mMarker!!.position, uri)
                    } else {
                        Log.d(logTag, "No image is received")
                    }
                } else {
                    Log.d(logTag, "Intent is null")
                }
        }
    }

    override fun onDestroy() {
        this.store.close()
        super.onDestroy()
    }


    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }

    private fun startLocationUpdates() {
        locationClient.requestLocationUpdates(locationRequest,
            locationCallback,
            Looper.getMainLooper())
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        locationClient.removeLocationUpdates(locationCallback)
    }

    private fun loadMarkersFromStore() {
        store.findAllMarkers()?.forEach {
            val position = LatLng(it.getLatitude(), it.getLongitude())
            mMarkersAndPhotos[position] = it.getImage()
            mMap.addMarker(
                MarkerOptions().position(position)
            )
        }
    }
}
