package com.example.map_lab123.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import java.io.File
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import android.os.*
import android.view.ViewGroup
import androidx.core.content.FileProvider
import com.example.map_lab123.R


@Suppress("DEPRECATION")
class MarkerActivity : AppCompatActivity() {
    private lateinit var imageUri: Uri
    private lateinit var imageView: ImageView

    private val makePhoto = 1
    private val currentImage = "CurrentImage"
    private val logTag = "marker-activity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_marker)
        imageView = ImageView(this)
        imageView.layoutParams = ViewGroup.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT
        )

        val linearLayout = findViewById<RelativeLayout>(R.id.layout)
        linearLayout?.addView(imageView)

        val uri = intent.getParcelableExtra<Uri>(currentImage)
        if (uri != null) {
            imageUri = uri
            openImage(imageUri)
        }

        val buttonSave = findViewById<Button>(R.id.button_save)
        buttonSave?.setOnClickListener {
            onClickSave()
        }

        val buttonPhoto = findViewById<Button>(R.id.button_photo)
        buttonPhoto?.setOnClickListener {
            onClickPhoto()
        }

        val buttonCancel = findViewById<Button>(R.id.button_cancel)
        buttonCancel?.setOnClickListener {
            onClickCancel()
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            outState.putParcelable(currentImage, imageUri as Parcelable)
            super.onSaveInstanceState(outState)
        } catch (e: Exception) {
            Log.e(logTag, "Save state error: $e.message")
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        try {
            val uri = savedInstanceState.getParcelable<Uri>(currentImage)
            if (uri != null) {
                imageUri = uri
                openImage(imageUri)
            }
        } catch (e: Exception) {
            Log.e(logTag, "Restore state error: $e.message")
        }
    }

    private fun onClickSave() {
        val output = Intent()
        output.putExtra(currentImage, imageUri)
        setResult(Activity.RESULT_OK, output)
        finish()
    }

    private fun onClickPhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        createFileImage()
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(intent, makePhoto)
    }

    private fun onClickCancel() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == makePhoto) {
            if (resultCode == Activity.RESULT_OK) {
                imageView.setImageURI(imageUri)
            } else {
                Log.d(logTag, "res code: $resultCode")
            }
        }
    }

    private fun openImage(uri: Uri) {
        imageView.setImageURI(uri)
    }

    private fun createFileImage() {
        val storageDir = /*Environment.*/getExternalFilesDir(
            Environment.DIRECTORY_PICTURES
        )
        val image = File.createTempFile(
            generateNewFilename(),
            ".jpg",
            storageDir
        )
        imageUri = FileProvider.getUriForFile(this, "com.example.map_lab123.fileprovider", image)
                //Uri.parse("file: $image.absolutePath")
       // val photoURI: Uri =

    }

    @SuppressLint("SimpleDateFormat")
    private fun generateNewFilename(): String {
        return "photo_" + SimpleDateFormat("yyyyMMdd_HHmm").format(Date())
    }
}
